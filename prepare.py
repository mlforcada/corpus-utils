#  transform.py
#  
#  Copyright 2019 Mikel L. Forcada
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  Lee un corpus paralelo en "formato Moses" de OPUS y genera
#  corpus train.*, dev.*, test.* del tamaño deseado
#  y un vocabulario para cada corpus de entrenamiento  
#  Permite usar opciones de "tokenización", "truecasing" y "subpalabras" (BPE)
#
#

import argparse   # módulo para leer los argumentos de la línea de orden
import random     # módulo de números aleatorios
import functools  # módulo para redefinir funciones
from collections import Counter # para contar las palabras de mayor frecuencia

# 'tokenizador' y "truecaser" (instalando sacremoses)
from sacremoses import MosesTokenizer, MosesTruecaser
# mt = MosesTokenizer()

# Necesitamos llamar al BPE como subproceso
import subprocess

# Para borrar temporales
import os

# Nos aseguramos de que todos los open() se realizan en utf-8
# redefiniendo open (esto hace falta porque si no, el programa
# usa el "encoding" nativo de la plataforma donde se ejecuta
open = functools.partial(open, encoding='utf8') 

# Creamos un analizador de los argumentos de la línea de órdenes
parser = argparse.ArgumentParser()

# Exigimos los argumentos:
# prefijo, primera lengua, segunda lengua, número de segmentos de "train",
# número de segmentos de "development" y número de segmentos de "test".

parser.add_argument("prefix",help="File prefix")  # El prefijo
parser.add_argument("l1", help="Language 1")      # La primera lengua
parser.add_argument("l2", help="Language 2")      # La segunda lengua
parser.add_argument("trainsize", help="Number of training segments", type=int)
parser.add_argument("devsize", help="Number of development segments", type=int)
parser.add_argument("testsize", help="Number of test segments", type=int	)
parser.add_argument("vocsize", help="Vocabulary size", type=int	)
parser.add_argument('--tokenize', action='store_true', help="Tokenize text") # Se 'tokenizará'
parser.add_argument('--truecase', action='store_true', help="Truecase text") # Se pasará todo a la forma más común 
parser.add_argument('--bpe', action='store_true', help="Train a BPE model and apply it") # Se entrenará un modelo BPE y se aplicará
# con el tamaño de vocabulario indicado por vocsize
# Leemos los argumentos exigidos (saca error si no están todos)

args=parser.parse_args()

# Ahora abrimos los ficheros y los leemos línea a línea conjuntamente
# en un "zip"


# Primer fichero tiene como nombre el prefijo, punto y l1
textfile1=open(args.prefix+"."+args.l1)

# Segundo fichero tiene como nombre el prefijo, punto y l2
textfile2=open(args.prefix+"."+args.l2)

# Cremallera "zip" con los dos ficheros, convertida en lista
corpus=list(zip(textfile1, textfile2))

# Lo ordenamos de alguna manera fija
sorted(corpus) 

# Y extraemos, aleatoriamente, training, dev y test.
# Pero antes:
random.seed(42) # fijamos una manera concreta de desordenar
random.shuffle(corpus) # Desordenamos el corpus de esa manera

# Calculamos los puntos de corte
split_1 = args.trainsize
split_2 = split_1 + args.devsize
split_3 = split_2 + args.testsize

# Y cogemos las rodajas correspondientes
train_corpus = corpus[:split_1]
dev_corpus    = corpus[split_1:split_2]
test_corpus     = corpus[split_2:split_3]

# Finalmente, toca escribir los ficheros en bruto

for corpus,prefix in ((train_corpus,"train."), (dev_corpus, "dev."), (test_corpus, "test.")):
	file1=open(prefix+args.l1,"w")
	file2=open(prefix+args.l2,"w")
	for pair in corpus:
		file1.write(pair[0])
		file2.write(pair[1])
	file1.close();
	file2.close();    

# Si se debe 'tokenizar', se tokeniza

changes_applied=""

if args.tokenize :
	for prefix in ["train.", "dev.", "test."] :
		for suffix in [args.l1, args.l2] :
			mt=MosesTokenizer(lang=suffix) # Para el francés y el italiano, apóstrofo
			with open(prefix+"tok."+suffix,"w") as outfile :
				[outfile.write(mt.tokenize(line,return_str=True)+"\n") for line in open(prefix+suffix)]
	changes_applied="tok."
	
	print("Ficheros 'tokenizados'")###DEBUG

# Y si se debe pasar a "truecase", se pasa # ARREGLAR
if args.truecase:

# Primero entrenamos modelos de truecasing basados los train, uno para cada lengua
# Los guardamos y los aplicamos llamando a sacremoses en un shell
 	
	for suffix in [args.l1, args.l2] :
		mtc = MosesTruecaser()
		mtc.train([line.split() for line in open("train."+changes_applied+suffix)], save_to="truecasemodel."+suffix)
		for prefix in ["train.", "dev.", "test."] :
			with open(prefix+changes_applied+"true."+suffix,"w") as outfile :
				[outfile.write(mtc.truecase(line, return_str=True)+"\n") for line in open(prefix+changes_applied+suffix)]
	changes_applied=changes_applied+"true."
	
	print("Ficheros 'truecaseados'")###DEBUG


if args.bpe :
	# Entrena BPE a partir de train	
	# Concatena los ficheros de entrenamiento en "temporary"
	tempfile=open("temporary","w")
	for suffix in [args.l1, args.l2] :
		[tempfile.write(line) for line in open("train."+changes_applied+suffix)]
	tempfile.close()
	subprocess.call("subword-nmt learn-bpe -s {0} < temporary > bpecodes.{1}-{2}".format(args.vocsize,args.l1,args.l2), shell=True) 
	os.remove("temporary")
	
	print("BPE entrenado")###DEBUG


	# Transforma todos los ficheros a BPE
	for prefix in ["train.", "dev.", "test."]:
		for suffix in [args.l1, args.l2] :
			_infile=prefix+changes_applied+suffix
			_outfile=prefix+changes_applied+"bpe."+suffix
			subprocess.call("subword-nmt apply-bpe -c bpecodes.{2}-{3} < {0} > {1}".format(_infile,_outfile,args.l1,args.l2),shell=True)	
	changes_applied=changes_applied+"bpe." 
	
	print("Ficheros pasados a BPE")###DEBUG


# end if args.bpe

# Generar vocabularios

if args.bpe :
	for suffix in [args.l1,args.l2] :
		_temp="temporary" # fichero temporal
		subprocess.call("subword-nmt get-vocab --input {0} --output {1}".format("train."+changes_applied+suffix,_temp), shell=True)
		outfile=open("vocab."+suffix, "w")
		outfile.write("<unk>\n")
		outfile.write("<s>\n")
		outfile.write("</s>\n")
		[outfile.write((line.split())[0]+"\n") for line in open(_temp)]
		os.remove(_temp) # Borra temporal
		
		print("Vocabularios BPE escritos")###DEBUG


else : # No BPE  # can probably use get-vocab as above
	for suffix in [args.l1, args.l2] :
		with open("train."+changes_applied+suffix) as infile :
			counter = Counter(infile.read().strip().split()) # óptimo si se "tokeniza"
		vocab=open("vocab."+changes_applied+suffix, "w")
		vocab.write("<unk>\n")
		vocab.write("<s>\n")
		vocab.write("</s>\n")
		for entrada in (counter.most_common())[:args.vocsize]: # y escribimos en el fichero
			vocab.write(entrada[0]+"\n"); # el primer elemento es la palabra (el segundo, 1, es la cuenta)
		vocab.close()
		
		print("Vocabularios normales escritos")###DEBUG

    


