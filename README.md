# corpus-utils

Útiles para gestionar corpus en traducción automática estadística, desarrollados
como parte del trabajo de final de grado de Clara García Abiétar (2019). Pensados
para funcionar en GNU/Linux y en Windows 10.

## prepare.py

Toma un corpus de [OPUS](http://opus.nlpl.eu) y produce ficheros de entrenamiento, 
desarrollo y test preprocesados de los tamaños deseados, cuyos pares de segmentos
han sido elegidos aleatoriamente.

Requiere que se hayan instalado [sacremoses](https://github.com/alvations/sacremoses) 
y [subword-nmt](https://github.com/rsennrich/subword-nmt).

El 'prefix' es el nombre de los ficheros de texto sin la terminación de lengua
(por ejemplo, sin `.fr` si es francés).

```
usage: prepare.py [-h] [--tokenize] [--truecase] [--bpe]
                  prefix l1 l2 trainsize devsize testsize vocsize

positional arguments:
  prefix      File prefix
  l1          Language 1
  l2          Language 2
  trainsize   Number of training segments
  devsize     Number of development segments
  testsize    Number of test segments
  vocsize     Vocabulary size

optional arguments:
  -h, --help  show this help message and exit
  --tokenize  Tokenize text
  --truecase  Truecase text
  --bpe       Train a BPE model and apply it

```

